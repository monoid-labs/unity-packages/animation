using UnityEngine;
using UnityEngine.Animations;
#if !(UNITY_2019_3_OR_NEWER)
using UnityEngine.Experimental.Animations;
#endif
using UnityEngine.Playables;

namespace Monoid.Unity.Animation.Samples {

  [RequireComponent(typeof(Animator))]
  public class StateAnimator : MonoBehaviour {
    public AnimationStates states;
    public bool defaultPoseBlending;

    Animator animator;
    PlayableGraph graph;
    AnimationPlayableOutput output;
    AnimationStatesBehaviour actions;

    void Awake() {
      animator = GetComponent<Animator>();

      graph = PlayableGraph.Create(animator.name);

      output = AnimationPlayableOutput.Create(graph, "output", animator);
      var playable = states.CreatePlayable(graph, gameObject);
      actions = AnimationStates.GetBehaviour(playable);
      output.SetSourcePlayable(playable);

      animator.ResolveAllStreamHandles();

      graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
      graph.Play();

    }

    void Update() {
      actions.DefaultPoseBlending = defaultPoseBlending;
    }

    void OnDestroy() {
      graph.Destroy();
    }

    public IAnimationPlayer Player => actions;
  }

}


