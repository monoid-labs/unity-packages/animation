﻿using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Animation.Samples {

  [CustomEditor(typeof(StateAnimator))]
  public class StateAnimatorEditor : Editor {

    float fade = 1.0f;
    // foldouts
    bool showTransition = true;
    bool showCurrent = true;
    bool showNext = true;

    public override void OnInspectorGUI() {
      DrawDefaultInspector();

      if (!Application.isPlaying) {
        return;
      }

      var t = target as StateAnimator;
      if (!t) {
        return;
      }

      EditorGUILayout.Separator();

      EditorGUILayout.LabelField("State", EditorStyles.boldLabel);
      showTransition = EditorGUILayout.Foldout(showTransition, "Transition");
      if (showTransition) {
        EditorGUI.indentLevel++;
        var transition = t.Player.Transition;
        EditorGUILayout.Slider("Progress", transition.progress, 0, 1);
        showCurrent = EditorGUILayout.Foldout(showCurrent, "Current");
        if (showCurrent) {
          EditorGUI.indentLevel++;
          var current = transition.current;
          EditorGUILayout.IntField("Id", current.id);
          EditorGUILayout.Slider("Progress", current.progress, 0, 1);
          EditorGUILayout.FloatField("Speed", current.speed);
          EditorGUI.indentLevel--;
        }
        showNext = EditorGUILayout.Foldout(showNext, "Next");
        if (showNext) {
          EditorGUI.indentLevel++;
          var next = transition.next;
          EditorGUILayout.IntField("Id", next.id);
          EditorGUILayout.Slider("Progress", next.progress, 0, 1);
          EditorGUILayout.FloatField("Speed", next.speed);
          EditorGUI.indentLevel--;
        }
        EditorGUI.indentLevel--;
      }

      EditorGUILayout.Separator();

      EditorGUILayout.LabelField("Testing", EditorStyles.boldLabel);
      fade = EditorGUILayout.Slider("Fade", fade, 0.0f, 5.0f);


      if (GUILayout.Button("- none -")) {
        t.Player.CrossFadeInFixedTime(-1, fade);
      }
      var states = t.states.states;
      for (int i = 0, n = states.Length; i < n; ++i) {
        var clip = states[i];
        var name = clip ? clip.name : "????";
        if (GUILayout.Button(name)) {
          t.Player.CrossFadeInFixedTime(i, fade);
        }
      }

      Repaint();
    }
  }

}
