using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Animations;

namespace Monoid.Unity.Animation {

  public struct AnimatorStates {

    public AnimatorStateInfo current, next;

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Refresh(Animator animator, int layer) {
      current = animator.GetCurrentAnimatorStateInfo(layer);
      next = animator.GetNextAnimatorStateInfo(layer);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void Refresh(AnimatorControllerPlayable playable, int layer) {
      current = playable.GetCurrentAnimatorStateInfo(layer);
      next = playable.GetNextAnimatorStateInfo(layer);
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Sync(int name) {
      return Sync(name, 0.0f);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Sync(string name) {
      return Sync(name, 0.0f);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Sync(int name, float defaultValue) {
      if (current.IsName(name)) {
        return current.normalizedTime;
      }
      if (next.IsName(name)) {
        return next.normalizedTime;
      }
      return defaultValue;
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float Sync(string name, float defaultValue) {
      if (current.IsName(name)) {
        return current.normalizedTime;
      }
      if (next.IsName(name)) {
        return next.normalizedTime;
      }
      return defaultValue;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float SyncInFixedTime(int name) {
      return SyncInFixedTime(name, 0.0f);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float SyncInFixedTime(string name) {
      return SyncInFixedTime(name, 0.0f);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float SyncInFixedTime(int name, float defaultValue) {
      if (current.IsName(name)) {
        return current.normalizedTime * current.length;
      }
      if (next.IsName(name)) {
        return next.normalizedTime * next.length;
      }
      return defaultValue;
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public float SyncInFixedTime(string name, float defaultValue) {
      if (current.IsName(name)) {
        return current.normalizedTime * current.length;
      }
      if (next.IsName(name)) {
        return next.normalizedTime * next.length;
      }
      return defaultValue;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool AnyName(int name) {
      return current.IsName(name) || next.IsName(name);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool AnyName(string name) {
      return current.IsName(name) || next.IsName(name);
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool NoName(int name) {
      return !current.IsName(name) && !next.IsName(name);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool NoName(string name) {
      return !current.IsName(name) && !next.IsName(name);
    }

    //-------------------------------------------------------------------------


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool Before(int name, float progress) {
      return current.Before(name, progress) || next.Before(name, progress);
    }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool Before(string name, float progress) {
      return current.Before(name, progress) || next.Before(name, progress);
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool After(int name, float progress) {
      return current.After(name, progress) || next.After(name, progress);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public bool After(string name, float progress) {
      return current.After(name, progress) || next.After(name, progress);
    }

    //-------------------------------------------------------------------------

  }

}