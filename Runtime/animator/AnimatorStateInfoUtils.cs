using System.Runtime.CompilerServices;
using UnityEngine;

namespace Monoid.Unity.Animation {

  public static class AnimatorStateInfoUtils {

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsName(this AnimatorStateInfo state, int name) {
      return state.fullPathHash == name;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Before(this AnimatorStateInfo state, int name, float progress) {
      return state.IsName(name) && state.normalizedTime < progress;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Before(this AnimatorStateInfo state, string name, float progress) {
      return state.IsName(name) && state.normalizedTime < progress;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool After(this AnimatorStateInfo state, int name, float progress) {
      return state.IsName(name) && state.normalizedTime > progress;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool After(this AnimatorStateInfo state, string name, float progress) {
      return state.IsName(name) && state.normalizedTime > progress;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Started(this AnimatorStateInfo state, int name) {
      return state.IsName(name) && state.normalizedTime >= 0.0f;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Started(this AnimatorStateInfo state, string name) {
      return state.IsName(name) && state.normalizedTime >= 0.0f;
    }

    //-------------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Finished(this AnimatorStateInfo state, int name) {
      return state.IsName(name) && state.normalizedTime >= 1.0f;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Finished(this AnimatorStateInfo state, string name) {
      return state.IsName(name) && state.normalizedTime >= 1.0f;
    }

    //-------------------------------------------------------------------------
  }

}