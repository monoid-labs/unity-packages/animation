using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  public class BlendSpaceBehaviour1D : PlayableBehaviour {

    AnimationMixerPlayable mixer;
    AnimationClipPlayable[] children;
    float[] positions, speeds;
    bool interpolate;

    public float Position { get; set; }

    public Playable GetChild (int index) => children[index];

    public void Initialize (PlayableGraph graph, ScriptPlayable<BlendSpaceBehaviour1D> playable,
      AnimationClip[] clips, float[] positions, float[] speeds, bool interpolate) {
      var n = clips.Length;

      mixer = AnimationMixerPlayable.Create (graph, n);

      this.children = new AnimationClipPlayable[clips.Length];
      for (int i = 0; i < n; i++) {
        this.children[i] = AnimationClipPlayable.Create (graph, clips[i]);
        this.children[i].SetSpeed (clips[i].length == 0 ? 1 : clips[i].length);
        mixer.ConnectInput (i, children[i], 0);
      }

      playable.ConnectInput (0, mixer, 0, 1.0f);

      this.positions = positions;
      this.speeds = speeds;
      this.interpolate = interpolate;
    }

    public override void PrepareFrame (Playable playable, FrameData info) {
      if (children == null) {
        return;
      }

      var n = children.Length;
      if (n <= 0) {
        return;
      }

      var pos = Position;
      mixer.SetSpeed (1.0f);

      int low = -1, high = -1;
      for (int i = 0; i < n; i++) {
        mixer.SetInputWeight (i, 0.0f);
        var p = positions[i];
        if (p <= pos && (low < 0 || p > positions[low])) {
          low = i;
        }
        if (p >= pos && (high < 0 || p < positions[high])) {
          high = i;
        }
      }

      if (low < 0) {
        Set (pos, high);
      } else if (high < 0) {
        Set (pos, low);
      } else if (positions[low] == positions[high]) {
        if (interpolate) {
          if (low == high) {
            Set (pos, low);
          } else {
            Set (pos, low, 0.5f, high, 0.5f);
          }
        } else {
          Set (pos, low);
        }
      } else {
        var d = positions[high] - positions[low];
        var t = (pos - positions[low]) / d;
        if (interpolate) {
          if (low == high) {
            Set (pos, low);
          } else {
            if (pos != 0.0f) {
              if (positions[low] == 0.0f) {
                t = 1.0f;
              } else if (positions[high] == 0.0f) {
                t = 0.0f;
              }
            }
            Set (pos, low, 1.0f - t, high, t);
          }
        } else {
          int index;
          if (positions[low] == 0.0f) {
            index = pos == 0.0f ? low : high;
          } else if (positions[high] == 0.0f) {
            index = pos == 0.0f ? high : low;
          } else if (positions[low] <= 0.0f && positions[high] >= 0.0f) {
            index = pos < 0.0f ? low : high;
          } else {
            index = t < 0.5f ? low : high;
          }
          Set (pos, index);
        }
      }

      if (!playable.GetPropagateSetTime()) {
        return;
      }
      var time = playable.GetTime() * mixer.GetSpeed();
      mixer.SetTime(time);
      for (int i = 0; i < n; i++) {
        children[i].SetTime(time * children[i].GetSpeed());
      }
    }

    void Set (float speed, int a, float u, int b, float v) {
      var speedA = Speed (speed, a) / children[a].GetSpeed ();
      var speedB = Speed (speed, b) / children[b].GetSpeed ();
      mixer.SetSpeed (speedA * u + speedB * v);
      mixer.SetInputWeight (a, u);
      mixer.SetInputWeight (b, v);
    }

    void Set (float speed, int index) {
      mixer.SetSpeed (Speed (speed, index) / children[index].GetSpeed ());
      mixer.SetInputWeight (index, 1.0f);
    }

    float Speed (float speed, int index) {
      return positions[index] == 0.0f ? 1.0f : speeds[index]*speed / positions[index];
    }

  }
}
