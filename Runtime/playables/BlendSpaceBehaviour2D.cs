using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  public class BlendSpaceBehaviour2D : PlayableBehaviour {

    #region Triangle

    public struct Triangle {
      public int a, b, c;

      public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
      }

      public bool Inside(Vector2[] positions, Vector2 sample) {
        return Inside(positions[a], positions[b], positions[c], sample);
      }
      public static bool Inside(Vector2 a, Vector2 b, Vector2 c, Vector2 v) {
        a -= v;
        b -= v;
        c -= v;
        var ab = (a.x * b.y - a.y * b.x);
        var bc = (b.x * c.y - b.y * c.x);
        var ca = (c.x * a.y - c.y * a.x);

        var x = ab * bc;
        var y = bc * ca;
        var z = ca * ab;
        return x >= 0.0f && y >= 0.0f && z >= 0.0f;
      }
      public Vector3 Weights(Vector2[] positions, Vector2 sample) {
        return Weights(positions[a], positions[b], positions[c], sample);
      }
      // barycentric coordinates
      public static Vector3 Weights(Vector2 a, Vector2 b, Vector2 c, Vector2 v) {
        var ab = b - a;
        var ac = c - a;
        var av = v - a;
        var denom = (ab.x * ac.y - ac.x * ab.y);
        if (denom == 0.0f) {
          return Vector3.one / 3.0f;
        }
        var y = (ac.y * av.x - ac.x * av.y) / denom;
        var z = (ab.x * av.y - ab.y * av.x) / denom;
        return new Vector3(1.0f - y - z, y, z);
      }


      public Vector3 Nearest(Vector2[] positions, ref Vector3 w) {
        return Nearest(positions[a], positions[b], positions[c], ref w);
      }
      public static Vector3 Nearest(Vector3 a, Vector3 b, Vector3 c, ref Vector3 w) {
        if (w.x >= w.y) {
          w.y = 0.0f;
          if (w.x >= w.z) {
            w.z = 0.0f;
            w.x = 1.0f;
            return a;
          } else {
            w.x = 0.0f;
            w.z = 1.0f;
            return c;
          }
        } else {
          w.x = 0.0f;
          if (w.y >= w.z) {
            w.z = 0.0f;
            w.y = 1.0f;
            return b;
          } else {
            w.y = 0.0f;
            w.z = 1.0f;
            return c;
          }
        }
      }
    }

    #endregion

    public static Triangle[] Triangulate(Vector2[] positions) {
      var tris = new List<Delaunay2D.Triangle>();
      var n = Delaunay2D.Triangulate(positions, tris);
      var triangles = new Triangle[n];
      for (int i = 0; i < triangles.Length; i++) {
        triangles[i] = new Triangle(tris[i].a, tris[i].b, tris[i].c);
      }
      return triangles;
    }

    AnimationMixerPlayable mixer;
    AnimationClipPlayable[] children;
    Vector2[] positions;
    float[] speeds;
    bool interpolate;

    Triangle[] triangles;

    public Vector2 Position { get; set; }

    public Playable GetChild(int index) => children[index];

    public void Initialize(PlayableGraph graph, ScriptPlayable<BlendSpaceBehaviour2D> playable,
      AnimationClip[] clips, Vector2[] positions, float[] speeds, bool interpolate) {
      var n = clips.Length;

      mixer = AnimationMixerPlayable.Create(graph, n);

      this.children = new AnimationClipPlayable[clips.Length];
      for (int i = 0; i < n; i++) {
        this.children[i] = AnimationClipPlayable.Create(graph, clips[i]);
        this.children[i].SetSpeed(clips[i].length == 0 ? 1 : clips[i].length);
        mixer.ConnectInput(i, children[i], 0);
      }

      playable.ConnectInput(0, mixer, 0, 1.0f);

      this.positions = positions;
      this.speeds = speeds;
      this.interpolate = interpolate;

      this.triangles = Triangulate(positions);
      // Debug.Log(triangles.Length);
      // for(int i=0; i<triangles.Length; i++) {
      //   var tri = triangles[i];
      //   var a = positions[tri.a];
      //   var b = positions[tri.b];
      //   var c = positions[tri.c];
      //   Debug.Log ($"{i} ({tri.a}:{a}, {tri.b}:{b}, {tri.c}:{c})");
      // }
    }

    public override void PrepareFrame(Playable playable, FrameData info) {
      if (children == null) {
        return;
      }

      Sync();

      if (!playable.GetPropagateSetTime()) {
        return;
      }
      var time = playable.GetTime() * mixer.GetSpeed();
      mixer.SetTime(time);
      for (int i = 0; i < children.Length; i++) {
        children[i].SetTime(time * children[i].GetSpeed());
      }
    }

    void Sync() {
      var n = children.Length;
      if (n <= 0) {
        return;
      }

      var pos = Position;
      mixer.SetSpeed(1.0f);
      for (int i = 0; i < n; i++) {
        mixer.SetInputWeight(i, 0.0f);
      }

      if (n == 1) {
        Set(pos, 0);
        return;
      }
      if (n == 2) {
        SetLine(pos, 0, 1);
        return;
      }

      if (triangles.Length == 0) {
        var distance = float.PositiveInfinity;
        var nearest_index = -1;
        for (int i = 0; i < n; i++) {
          var d = Vector2.Distance(pos, positions[i]);
          if (d < distance) {
            nearest_index = i;
            distance = d;
          }
        }
        distance = float.PositiveInfinity;
        var second_nearest_index = -1;
        for (int i = 0; i < n; i++) {
          if (i == nearest_index) {
            continue;
          }
          var d = Vector2.Distance(pos, positions[i]);
          if (d < distance) {
            second_nearest_index = i;
            distance = d;
          }
        }
        SetLine(pos, nearest_index, second_nearest_index);
        return;
      }

      var w = Vector3.zero;

      var closest_triangle = -1;
      var closest_point = Vector2.one * float.PositiveInfinity;

      for (int i = 0; i < triangles.Length; i++) {
        var tri = triangles[i];
        var a = positions[tri.a];
        var b = positions[tri.b];
        var c = positions[tri.c];

        if (Triangle.Inside(a, b, c, pos)) {
          w = Triangle.Weights(a, b, c, pos);
          SetTriangle(pos, tri, w);
          return;
        }

        float t;
        if (OutsideCandidate(a, b, pos, ref closest_point, out t)) {
          w = new Vector3(1.0f - t, t, 0.0f);
          closest_triangle = i;
        }
        if (OutsideCandidate(b, c, pos, ref closest_point, out t)) {
          w = new Vector3(0.0f, 1.0f - t, t);
          closest_triangle = i;
        }
        if (OutsideCandidate(c, a, pos, ref closest_point, out t)) {
          w = new Vector3(t, 0.0f, 1.0f - t);
          closest_triangle = i;
        }
      }

      if (closest_triangle >= 0) { // should always be true
        var tri = triangles[closest_triangle];
        SetTriangle(pos, tri, w);
        // Debug.Log($"Outside: {closest_triangle} ({positions[tri.a]}, {positions[tri.b]}, {positions[tri.c]}) {w} {closest_point} {s}");
        return;
      }
    }

    void SetTriangle(Vector2 pos, Triangle tri, Vector3 w) {
      if (pos != Vector2.zero) {
        if (positions[tri.a] == Vector2.zero && w.x != 0.0f) {
          w.x = 0.0f;
          var s = w.y + w.z;
          w.y /= s;
          w.z /= s;
        }
        if (positions[tri.b] == Vector2.zero && w.y != 0.0f) {
          w.y = 0.0f;
          var s = w.x + w.z;
          w.x /= s;
          w.z /= s;
        }
        if (positions[tri.c] == Vector2.zero && w.z != 0.0f) {
          w.z = 0.0f;
          var s = w.x + w.y;
          w.x /= s;
          w.y /= s;
        }
      }
      if (interpolate) {
        Set(pos, tri.a, w.x, tri.b, w.y, tri.c, w.z);
        return;
      }
      int index = w.x >= w.y && (positions[tri.a] != Vector2.zero || pos == Vector2.zero) ?
        w.z >= w.x && (positions[tri.c] != Vector2.zero || pos == Vector2.zero) ? tri.c : tri.a :
        w.y >= w.z && (positions[tri.b] != Vector2.zero || pos == Vector2.zero) ? tri.b : tri.c;
      Set(pos, index);
    }

    void SetLine(Vector2 pos, int a, int b) {
      float t;
      var nearest = LineSegmentNearest(positions[a], positions[b], pos, out t);
      if (t <= 0.0f) {
        Set(pos, a);
        return;
      }
      if (t >= 1.0f) {
        Set(pos, b);
        return;
      }
      if (pos != Vector2.zero) {
        if (positions[a] == Vector2.zero) {
          Set(pos, b);
          return;
        }
        if (positions[b] == Vector2.zero) {
          Set(pos, a);
          return;
        }
      }
      if (interpolate) {
        Set(pos, a, 1.0f - t, b, t);
        return;
      }
      var index = a + Mathf.RoundToInt(t) * (b - a);
      Set(pos, index);
    }

    void Set(Vector2 velocity, int a, float u, int b, float v, int c, float w) {
      // Debug.Log($"Set3: {children[a].GetAnimationClip().name}[a]:{u} {children[b].GetAnimationClip().name}[{b}]:{v} {children[c].GetAnimationClip().name}[{c}]:{w}");
      var speedA = Speed(velocity, a) / children[a].GetSpeed();
      var speedB = Speed(velocity, b) / children[b].GetSpeed();
      var speedC = Speed(velocity, c) / children[c].GetSpeed();
      var speed = speedA * u + speedB * v + speedC * w;
      mixer.SetSpeed(speed);
      mixer.SetInputWeight(a, u);
      mixer.SetInputWeight(b, v);
      mixer.SetInputWeight(c, w);
    }

    void Set(Vector2 velocity, int a, float u, int b, float v) {
      // Debug.Log($"Set2: {children[a].GetAnimationClip().name}[a]:{u} {children[b].GetAnimationClip().name}[{b}]:{v}");
      var speedA = Speed(velocity, a) / children[a].GetSpeed();
      var speedB = Speed(velocity, b) / children[b].GetSpeed();
      var speed = speedA * u + speedB * v;
      mixer.SetSpeed(speed);
      mixer.SetInputWeight(a, u);
      mixer.SetInputWeight(b, v);
    }

    void Set(Vector2 velocity, int index) {
      // Debug.Log($"Set: {children[index].GetAnimationClip().name}");
      var speed = Speed(velocity, index) / (float)children[index].GetSpeed();
      mixer.SetSpeed(speed);
      mixer.SetInputWeight(index, 1.0f);
    }

    float Speed(Vector2 velocity, int index) {
      var speed = positions[index].sqrMagnitude;
      return speed == 0.0f ? 1.0f : speeds[index] * Vector2.Dot(velocity, positions[index]) / speed;
    }

    static bool OutsideCandidate(Vector2 a, Vector2 b, Vector2 v, ref Vector2 best_point, out float t) {
      var closest = LineSegmentNearest(a, b, v, out t);
      if (Vector2.Distance(v, closest) > Vector2.Distance(v, best_point)) {
        return false;
      }
      best_point = closest;
      return true;
    }

    static Vector2 LineSegmentNearest(Vector2 a, Vector2 b, Vector2 v, out float t) {
      var ab = b - a;
      var denom = ab.sqrMagnitude;
      if (denom == 0.0f) {
        t = 0.5f;
        return a;
      }

      var av = v - a;
      t = Vector2.Dot(ab, av) / denom;
      t = Mathf.Clamp01(t);
      return a + t * ab;
    }
  }

}
