using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  [CreateAssetMenu(fileName = "BlendSpace1D", menuName = "Playables/Animation/BlendSpace1D")]//, order = Package.MenuOrder)]
  public class BlendSpace1D : PlayableAsset {

    [Serializable]
    public struct State {
      public AnimationClip clip;
      public float position;
      public float speed;
    }

    public State[] states = { };
    public bool interpolate;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
      return CreatePlayable(in graph, owner, states, interpolate);
    }

    public static BlendSpaceBehaviour1D GetBehaviour(in Playable playable) {
      var script = (ScriptPlayable<BlendSpaceBehaviour1D>)playable;
      return script.GetBehaviour();
    }

    public static Playable CreatePlayable(in PlayableGraph graph, GameObject owner, State[] states, bool interpolate) {
      var n = states.Length;
      var clips = new AnimationClip[n];
      var positions = new float[n];
      var speeds = new float[n];
      for (int i = 0; i < n; i++) {
        clips[i] = states[i].clip;
        positions[i] = states[i].position;
        speeds[i] = states[i].speed;
      }

      var playable = ScriptPlayable<BlendSpaceBehaviour1D>.Create(graph, 1);
      playable.GetBehaviour().Initialize(graph, playable, clips, positions, speeds, interpolate);
      return playable;
    }

  }
}
