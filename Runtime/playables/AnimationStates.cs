using UnityEngine;
using UnityEngine.Playables;

namespace Monoid.Unity.Animation {

  [CreateAssetMenu(fileName = "States", menuName = "Playables/Animation/States")]//, order = Package.MenuOrder)]
  public class AnimationStates : PlayableAsset {

    public AnimationClip[] states = { };

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner) {
      var playable = ScriptPlayable<AnimationStatesBehaviour>.Create(graph, 1);
      playable.GetBehaviour().Initialize(graph, playable, states);
      return playable;
    }

    public static AnimationStatesBehaviour GetBehaviour(Playable playable) {
      var script = (ScriptPlayable<AnimationStatesBehaviour>)playable;
      return script.GetBehaviour();
    }

  }
}