using System;

namespace Monoid.Unity.Animation {

  [Serializable]
  public struct AnimationState {

    public static AnimationState None => new AnimationState { id = -1, speed = 1.0f };

    public int id;
    public float progress; // normalized time
    public float duration; // if speed == 1.0
    public float speed;    // can be < 0.0 for reverse playback
    // public bool loop;      // is clip looping?
  }

  public static class AnimationStateExtensions {
    public static bool Is(in this AnimationState state, int id) => (state.id == id);
    public static bool IsAny(in this AnimationState state, int idA, int idB) => (state.id == idA) | (state.id == idB);
    public static bool IsAny(in this AnimationState state, int idA, int idB, int idC) => (state.id == idA) | (state.id == idB) | (state.id == idC);
    public static bool IsAny(in this AnimationState state, int idA, int idB, int idC, int idD) => (state.id == idA) | (state.id == idB) | (state.id == idC) | (state.id == idD);
  }

}
