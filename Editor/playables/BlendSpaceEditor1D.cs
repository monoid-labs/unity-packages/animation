using System;
using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Animation {

  [CustomEditor(typeof(BlendSpace1D))]
  public class BlendSpaceEditor1D : Editor {

    // Based on: Unity's BlendTreeEditor's 1D case
    // https://github.com/Unity-Technologies/UnityCsReference/blob/54dd67f09e090b1ad5ba1f55886f327106406b0c/Editor/Mono/Inspector/BlendTreeInspector.cs

    static class Styles {
      public static readonly GUIStyle Background = "MeBlendBackground";
      public static readonly GUIStyle TriangleLeft = "MeBlendTriangleLeft";
      public static readonly GUIStyle TriangleRight = "MeBlendTriangleRight";

      public static readonly GUIContent Interpolate = new GUIContent("Interpolate");

      public static readonly GUIContent Clip = new GUIContent("Clip");
      public static readonly GUIContent Position = new GUIContent("Position");
      public static readonly GUIContent Speed = new GUIContent("Speed");
      public static readonly GUIContent BlendSpace = new GUIContent("Blend Space");
    }

    static readonly int BlendAnimationID = "BlendAnimationIDHash".GetHashCode();

    SerializedProperty children;
    UnityEditorInternal.ReorderableList reorderableList;

    public override void OnInspectorGUI() {
      if (children == null) {
        children = serializedObject.FindProperty("states");
        reorderableList = new UnityEditorInternal.ReorderableList(serializedObject, children) {
          drawHeaderCallback = ReorderableListHeader,
          drawElementCallback = ReorderableListElement,
          onAddCallback = ReorderableListElementAdd,
          onRemoveCallback = ReorderableListElementRemove
        };
      }

      GUI.enabled = false;
      EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));
      GUI.enabled = true;

      EditorGUILayout.Space();
      var space = target as BlendSpace1D;
      EditorGUILayout.LabelField(Styles.BlendSpace, EditorStyles.boldLabel);
      if (children.arraySize > 1) {
        BlendGraph(EditorGUILayout.GetControlRect(false, 40, Styles.Background));
      }

      EditorGUILayout.Space();
      reorderableList.DoLayoutList();

      EditorGUILayout.Space();

      EditorGUI.BeginChangeCheck();
      EditorGUILayout.PropertyField(serializedObject.FindProperty("interpolate"), Styles.Interpolate);
      if (EditorGUI.EndChangeCheck()) {
        serializedObject.ApplyModifiedProperties();
      }
    }

    void ReorderableListElementAdd(UnityEditorInternal.ReorderableList list) {
      var index = children.arraySize++;
      var child = children.GetArrayElementAtIndex(index);
      var speed = child.FindPropertyRelative("speed");
      speed.floatValue = 1.0f;
      serializedObject.ApplyModifiedProperties();
      //UnityEditorInternal.ReorderableList.defaultBehaviours.DoAddButton(list);
    }
    void ReorderableListElementRemove(UnityEditorInternal.ReorderableList list) {
      for (int i = list.index + 1, n = children.arraySize; i < n; i++) {
        children.MoveArrayElement(i, i - 1);
      }
      children.arraySize--;
      serializedObject.ApplyModifiedProperties();
      //UnityEditorInternal.ReorderableList.defaultBehaviours.DoRemoveButton(list);
    }

    void ReorderableListHeader(Rect rect) {
      var clipRect = rect.SliceX(rect.width * 0.6f, 4.0f);
      GUI.Label(clipRect, Styles.Clip, EditorStyles.label);
      var positionRect = rect.SliceX(rect.width * 0.5f, 4.0f);
      GUI.Label(positionRect, Styles.Position, EditorStyles.label);
      var speedRect = rect.SliceX(rect.width, 4.0f);
      GUI.Label(speedRect, Styles.Speed, EditorStyles.label);
    }

    void ReorderableListElement(Rect rect, int index, bool isActive, bool isFocused) {
      rect.y++;
      rect.height = 16.0f;

      var child = children.GetArrayElementAtIndex(index);
      var clip = child.FindPropertyRelative("clip");
      var position = child.FindPropertyRelative("position");
      var speed = child.FindPropertyRelative("speed");

      var clipRect = rect.SliceX(rect.width * 0.6f, 4.0f);
      EditorGUI.PropertyField(clipRect, clip, GUIContent.none);
      var positionRect = rect.SliceX(rect.width * 0.5f, 4.0f);
      EditorGUI.PropertyField(positionRect, position, GUIContent.none);
      var speedRect = rect.SliceX(rect.width, 4.0f);
      EditorGUI.PropertyField(speedRect, speed, GUIContent.none);

      serializedObject.ApplyModifiedProperties();
    }

    void BlendGraph(Rect area) {
      var n = children.arraySize;
      var positions = new float[n];
      var map = new int[n];
      for (int i = 0; i < n; i++) {
        var child = children.GetArrayElementAtIndex(i);
        var position = child.FindPropertyRelative("position");
        positions[i] = position.floatValue;
        map[i] = i;
      }
      Array.Sort(positions, map);

      var min = Mathf.Min(positions);
      var max = Mathf.Max(positions);
      for (int i = 0; i < positions.Length; i++) {
        positions[i] = area.x + Mathf.InverseLerp(min, max, positions[i]) * area.width;
      }

      var controlID = GUIUtility.GetControlID(BlendAnimationID, FocusType.Passive);

      var evt = Event.current;
      switch (evt.GetTypeForControl(controlID)) {
        case EventType.Repaint:
          Styles.Background.Draw(area, GUIContent.none, false, false, false, false);
          if (n > 1) {
            for (int i = 0; i < positions.Length; i++) {
              // draw the animation triangle
              var last = (i == 0) ? positions[i] : positions[i - 1];
              var next = (i == positions.Length - 1) ? positions[i] : positions[i + 1];
              var selected = reorderableList.index == map[i] ? true : false;
              DrawAnimation(positions[i], last, next, selected, area);
            }
          }
          break;
        case EventType.MouseDown:
          if (area.Contains(evt.mousePosition)) {
            evt.Use();
            GUIUtility.hotControl = controlID;
            GUIUtility.keyboardControl = controlID;
            // closest position
            var clickPosition = evt.mousePosition.x;
            var distance = Mathf.Infinity;
            for (int i = 0; i < positions.Length; i++) {
              var last = (i == 0) ? positions[i] : positions[i - 1];
              var next = (i == positions.Length - 1) ? positions[i] : positions[i + 1];
              var d = Mathf.Abs(clickPosition - positions[i]);
              if (d < distance && clickPosition < next && clickPosition > last) {
                distance = d;
                reorderableList.index = i;
              }
            }
          }
          break;
        case EventType.MouseUp:
          if (GUIUtility.hotControl == controlID) {
            evt.Use();
            GUIUtility.hotControl = 0;
            reorderableList.index = -1;
          }
          break;
      }
    }

    static void DrawAnimation(float val, float min, float max, bool selected, Rect area) {
      var top = area.y;
      var leftRect = new Rect(min, top, val - min, area.height);
      var rightRect = new Rect(val, top, max - val, area.height);
      Styles.TriangleLeft.Draw(leftRect, selected, selected, false, false);
      Styles.TriangleRight.Draw(rightRect, selected, selected, false, false);
      area.height -= 1;
      var oldColor = Handles.color;
      var newColor = selected ? new Color(1f, 1f, 1f, 0.6f) : new Color(1f, 1f, 1f, 0.4f);
      Handles.color = newColor;
      if (selected) {
        Handles.DrawLine(new Vector3(val, top, 0), new Vector3(val, top + area.height, 0));
      }
      var points = new Vector3[2] { new Vector3(min, top + area.height, 0f), new Vector3(val, top, 0f) };
      Handles.DrawAAPolyLine(points);
      points = new Vector3[2] { new Vector3(val, top, 0f), new Vector3(max, top + area.height, 0f) };
      Handles.DrawAAPolyLine(points);
      Handles.color = oldColor;
    }
  }
}