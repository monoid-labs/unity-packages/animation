﻿using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Monoid.Unity.Animation {

  public static class AnimatorControllerEditorUtility {

    const int MenuPriority = 222;

    #region Apply Overrides / Replace Animations

    const string MenuApplyOverrides = "Assets/Animations/Apply Overrides";

    [MenuItem(MenuApplyOverrides, true, MenuPriority)]
    public static bool CanApplyOverrides() {
      var overrides = Selection.activeObject as AnimatorOverrideController;
      if (!overrides) {
        return false;
      }
      var controller = overrides.runtimeAnimatorController as AnimatorController;
      if (!controller) {
        return false;
      }
      return true;
    }

    [MenuItem(MenuApplyOverrides, false, MenuPriority)]
    public static void ApplyOverrides() {
      var controller = Selection.activeObject as AnimatorOverrideController;
      if (!controller) {
        Debug.LogError($"{nameof(AnimatorControllerEditorUtility)}.{nameof(ApplyOverrides)}: Active selection is not an {nameof(AnimatorOverrideController)}.", controller);
        return;
      }
      ApplyOverrides(controller);
    }

    public static void ApplyOverrides(AnimatorOverrideController overrides) {
      var controller = overrides.runtimeAnimatorController as AnimatorController;
      if (!controller) {
        Debug.LogError($"{nameof(AnimatorControllerEditorUtility)}.{nameof(ApplyOverrides)}: Cannot use {nameof(AnimatorOverrideController)}.{nameof(overrides.runtimeAnimatorController)} as {nameof(AnimatorController)}", overrides);
        return;
      }
      ReplaceAnimations(controller, NewAnimationClipRegistry(overrides));
    }

    /*public*/
    delegate AnimationClip AnimationClipRegistry(string name);

    /*public*/
    static AnimationClipRegistry NewAnimationClipRegistry(AnimatorOverrideController overrides) => (string name) => overrides[name];

    /*public*/
    static void ReplaceAnimations(AnimatorController controller, AnimationClipRegistry animations) {
      foreach (var layer in controller.layers) {
        ReplaceAnimations(layer.stateMachine, animations);
      }
      EditorUtility.SetDirty(controller);
    }

    static void ReplaceAnimations(AnimatorStateMachine stateMachine, AnimationClipRegistry animations) {
      foreach (var state in stateMachine.states) {
        ReplaceAnimations(state.state, animations);
      }
      foreach (var childStateMachine in stateMachine.stateMachines) {
        ReplaceAnimations(childStateMachine.stateMachine, animations);
      }
    }

    static void ReplaceAnimations(AnimatorState state, AnimationClipRegistry animations) {
      var motion = state.motion;
      if (!motion) {
        return;
      }
      var clip = motion as AnimationClip;
      if (clip) {
        var animation = animations(clip.name);
        if (animation) {
          state.motion = animation;
        }
        return;
      }

      var blend = motion as BlendTree;
      if (blend) {
        ReplaceAnimations(blend, animations);
        return;
      }
    }

    static Motion ReplaceAnimations(Motion motion, AnimationClipRegistry animations) {
      var clip = motion as AnimationClip;
      if (clip) {
        var animation = animations(clip.name);
        if (animation) {
          return animation;
        }
        return motion;
      }

      var blend = motion as BlendTree;
      if (blend) {
        var children = blend.children;
        for (int i = 0; i < children.Length; i++) {
          children[i].motion = ReplaceAnimations(children[i].motion, animations);
        }
        blend.children = children;
        return motion;
      }

      return motion;
    }


    #endregion

  }


}
